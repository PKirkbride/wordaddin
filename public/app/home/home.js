(function(){
  'use strict'

  // The initialize function must be run each time a new page is loaded
  Office.initialize = function(reason){
    jQuery(document).ready(function(){
      app.initialize()
      // jQuery('#get-data-from-selection').click(getDataFromSelection)
      jQuery('#get-data-from-selection').click(getFile)
    })
  }
  function getDataFromSelection(){
    Word.run(function(context) {
          // Insert your code here. For example:
          var documentBody = context.document.body;
          context.load(documentBody);
          return context.sync()
          .then(function(){
              console.dir(documentBody);
          })
      });
  }

  function getFile(){
          Office.context.document.getFileAsync(Office.FileType.Text, { sliceSize: 4194304  /*64 KB*/ },
            function (result) {
                if (result.status == "succeeded") {
                    // If the getFileAsync call succeeded, then
                    // result.value will return a valid File Object.
                    var myFile = result.value;
                    console.dir(myFile.toSource())
                    // left off here | before told to stop
                    // var blob = new Blob(
                    var sliceCount = myFile.sliceCount;
                    var slicesReceived = 0, gotAllSlices = true, docdataSlices = [];
                    app.showNotification("File size:" + myFile.size + " #Slices: " + sliceCount);

                    // Get the file slices.
                    getSliceAsync(myFile, 0, sliceCount, gotAllSlices, docdataSlices, slicesReceived);
                }
                else {
                    app.showNotification("Error:", result.error.message);
                }
            });
      }


      function getSliceAsync(file, nextSlice, sliceCount, gotAllSlices, docdataSlices, slicesReceived) {
          file.getSliceAsync(nextSlice, function (sliceResult) {
              if (sliceResult.status == "succeeded") {
                  if (!gotAllSlices) { // Failed to get all slices, no need to continue.
                      return;
                  }

                  // Got one slice, store it in a temporary array.
                  // (Or you can do something else, such as
                  // send it to a third-party server.)
                  docdataSlices[sliceResult.value.index] = sliceResult.value.data;
                  if (++slicesReceived == sliceCount) {
                      // All slices have been received.
                      file.closeAsync();
                      console.log(docdataSlices); // docDataSlices contains all the text....
                  }
                  else {
                      getSliceAsync(file, ++nextSlice, sliceCount, gotAllSlices, docdataSlices, slicesReceived);
                  }
              }
              else {
                  gotAllSlices = false;
                  file.closeAsync();
                  app.showNotification("getSliceAsync Error:", sliceResult.error.message);
              }
          });
  }

  // Reads data from current document selection and displays a notification
  function getDataFromSelectionZZZ(){
    Office.context.document.getSelectedDataAsync(Office.CoercionType.Text,
      function(result){
      jQuery('#get-data-from-selection').click(getDataFromSelection)
        if (result.status === Office.AsyncResultStatus.Succeeded) {
          app.showNotification('The selected text is:', '"' + result.value + '"');
        } else {
          app.showNotification('Error:', result.error.message);
        }
      }
    );
  }

})();
